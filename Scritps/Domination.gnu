#!/usr/bin/gnuplot

# Make gnuplot otuput directly to pdf.
set terminal pdf monochrome dashed
set output "Domination.pdf" # Set output file name.

# Set axes style.
set style line 11 linetype 1 # Define border style.
set border 3 linestyle 11 # Set bottom and left border style.
set tics nomirror out scale 0.75 # Make graph tics smaller.

# Grid lines style.
set style line 12 linetype 3 linewidth 1
set grid linestyle 12

# Set axes range.
set xrange [0:1]
set yrange [0:8]

# Set axes labels.
set xlabel 'Kriterij A' offset 0.0, 0.75
set ylabel 'Kriterij B' offset 0.5, 0.0

# Hollow black circles. This is for the nondominated data.
set style line 1 pointtype 6 pointsize 1 linetype 1 linewidth 2

# Black crosses. This is style for dominated data.
set style line 2 pointtype 2 pointsize 1 linetype  1 linewidth 2

# Set legend font.
set key font 'sans, 12'

# Plot the data.
plot 'Nondominated.txt' with points linestyle 1 title 'nedominirano', \
     'Dominated.txt'    with points linestyle 2 title 'dominirano'
