% ----------------------------- CLASS INFORMATION ------------------------------
% Version of LaTeX to use.
\NeedsTeXFormat{LaTeX2e}

 % Name of the class.
\newcommand{\@classname}{paper}
\ProvidesClass{\@classname}
% ------------------------------------------------------------------------------


% --------------------------- GENERAL DOCUMENT STYLE ---------------------------
% Document class to build this class upon.
\LoadClass[10pt, a4paper, oneside, twocolumn]{article}

% Main font (Latin Modern).
\RequirePackage{lmodern}

% Monospace font (teletypewriter font).
\RequirePackage{courier}

% Enable hyperlinks on references, but show them only on mouse hover.
\RequirePackage[hidelinks]{hyperref}
% ------------------------------------------------------------------------------


% --------------------------- LANGUAGE AND ENCODING ----------------------------
% Encoding of the document source.
\RequirePackage[utf8]{inputenc}

% Document language.
\RequirePackage[english]{babel}

% Better support for quotation signs (context and language aware).
\RequirePackage{csquotes}

% Encode fonts in a modern way. Works good with non-English characters.
\RequirePackage[T1]{fontenc}

% Enable extra math symbols.
\RequirePackage{amssymb}
% ------------------------------------------------------------------------------


% --------------------------- DOCUMENT TITLE STYLE -----------------------------
% Package that enables manipulation of the title style.
\RequirePackage{titling}

% Reduce space before title. 
\setlength{\droptitle}{-100pt}

% Make title bold.
\renewcommand{\maketitlehooka}{ \begin{bfseries} }
\renewcommand{\maketitlehookb}{ \end{bfseries} }
% ------------------------------------------------------------------------------


% --------------------------- SECTION TITLE STYLE ------------------------------
% Enable formatting of section titles.
\RequirePackage{titlesec}

% Set spacing before and after sections and subsections.
\titlespacing*{\section}         % Set section spacing.
    {0pt}                        % Left spacing
    {2.0ex plus 1ex minus .2ex}  % Spacing before.
    {1.0ex plus .1ex}            % Spacing after.
\titlespacing*{\subsection}      % Set subsection spacing.
    {0pt}                        % Left spacing.
    {1.5ex plus .5ex minus .1ex} % Spacing before.
    {1ex plus .1ex}              % Spacing after.

% Format the section title.
\titleformat{\section}
    {\center\large\scshape} % Title font style.
    {\thesection.}          % Title label (number) style.
    {1.5ex}                 % Spacing between label (number) and title.
    {}                      % Commands to execute before title.

% Format the subsection title.
\titleformat{\subsection}
    {\center\normalsize\scshape} % Title font style.
    {\thesubsection.}            % Title label (number) style.
    {1.1ex}                      % Spacing between label (number) and title.
    {}                           % Commands to execute before title.
% ------------------------------------------------------------------------------


% ----------------------- AUTHORS AND AFFILIATIONS -----------------------------
% Enable authors and affiliations block.
\RequirePackage{authblk}

% Set author and affiliation fonts.
\renewcommand{\Authfont}{\large}
\renewcommand{\Affilfont}{\itshape\normalsize}

% Spacing between authors and affiliations.
\setlength{\affilsep}{7pt}

% Delimiter between two authors (when there are exactly two).
\renewcommand{\Authand}{ and }

% Delimiter between two authors that are not last (when there more than two).
\renewcommand{\Authsep}{, }

% Delimiter between last two authors (when there are more than two).
\renewcommand{\Authands}{ and }
% ------------------------------------------------------------------------------


% ------------------------------- NUMBERING ------------------------------------
% Enable modifying numbering counters.
\RequirePackage{chngcntr}

% Number document elements from within the section.
\counterwithin{figure}{section}   % Figure numbering.
\counterwithin{equation}{section} % Equation numbering.
\counterwithin{table}{section}    % Table numbering.
% ------------------------------------------------------------------------------


% ------------------------------ ABSTRACT STYLE --------------------------------
% Renew the abstract environment.
\renewenvironment{abstract}
{  
    \textsc{Abstract} \hspace{7pt} --
    \begin{bfseries}\begin{footnotesize}
}
{
    \end{footnotesize}\end{bfseries}
}
% ------------------------------------------------------------------------------


% --------------------------------- CAPTIONS -----------------------------------
% Caption style.
\RequirePackage{caption} % Enable captions.
\renewcommand{\captionlabelfont}{\bfseries}
\renewcommand{\captionfont}{\small}
% ------------------------------------------------------------------------------


% -------------------------------- BIBLIOGRAPHY --------------------------------
% Package to use for managing bibliography. 
\RequirePackage[citestyle=numeric,
                bibstyle=numeric,
                backend=biber,
                sorting=none]{biblatex}
               
% Remove "in:" before year for some source types.
\renewbibmacro{in:}{}

% Remove parenthesis around year for some source types.
\renewbibmacro*{issue+date}{
    \setunit{\addcomma\space}
    \iffieldundef{issue}
        {
            \usebibmacro{date}
        }
        {
            \printfield{issue}
            \setunit*{\addspace}
            \usebibmacro{date}
        }
    \newunit
}

% Separator between bibliography sections (for example, author and title).
\renewcommand{\newunitpunct}{\addcomma\space}

% Display first and last name of the author in small capital letters.
\renewcommand{\mkbibnamegiven}[1]{\textsc{#1}}
\renewcommand{\mkbibnamefamily}[1]{\textsc{#1}}

% Title of the work displayed in bold font.
\DeclareFieldFormat*{title}{\textbf{#1}}
% ------------------------------------------------------------------------------


% ---------------- MANDATORY AND OPTIONAL DOCUMENT INFORMATION -----------------
% Make document date optional.
\date{} % Define empty date (no date is defined).
% ------------------------------------------------------------------------------


% ---------------------------------- IMAGES ------------------------------------
% Enable image importing.
\RequirePackage{graphicx}

% Enable text-like positioning behavior for figures.
\RequirePackage{float}

% Command for inserting standard figure. This figure can be moved around the
% document by LaTeX to better fill the document space. LaTeX usually places
% figures at the beginning or the end of the page.
\newcommand{\standardfigure}[4]
{
    \begin{figure}
        \centering
        \includegraphics[#2]{#1}
        \captionsetup{justification=centering}
        \caption{#3}
        \label{#4}
    \end{figure}
}

% Command for inserting floating figure. Floating means that the figure is
% inserted at the exact position where it is in the document source - LaTeX will
% not move it around to better fill the document.
\newcommand{\floatingfigure}[4]
{
    \begin{figure}[H]
        \centering
        \includegraphics[#2]{#1}
        \captionsetup{justification=centering}
        \caption{#3}\label{#4}
    \end{figure}
}
% ------------------------------------------------------------------------------


% ----------------------------------- LISTS ------------------------------------
% Package that enables customization.
\RequirePackage{enumitem}

% Enumerate with numbers and brace.
\setenumerate[1]{label={\arabic*)}}

% Remove vertical spacing.
\setlist{noitemsep}
% ------------------------------------------------------------------------------


% ------------------------------ HELPER COMMANDS -------------------------------
% Disable hyphenation. Must be invoked after begin document.
\newcommand{\disablehyphenation} {
    \righthyphenmin=62 % Minimum number of letters after hyphenation.
    \lefthyphenmin=62  % Minimum number of letters before hyphenation.
}

% Reduce the amount of hyphenation. Must be invoked after begin document.
\newcommand{\reducehyphenation} {
    \righthyphenmin=5 % Minimum number of letters after hyphenation.
    \lefthyphenmin=5  % Minimum number of letters before hyphenation.
}
% ------------------------------------------------------------------------------
